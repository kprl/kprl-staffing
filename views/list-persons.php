<?php 
	
if ( $PoT == TRUE ) {
	
	if ( $wrap ) { echo '<div id="' . $cssss . '-0" class="kprl-staffing-list ' . $cssss . '">'; }
	
	if ( $a['heading'] !== NULL ) {
		if ( $a['headingid'] !== NULL ) {
			$slug = sanitize_title( $a['headingid'] );
		} else {
			$slug = sanitize_title( $a['heading'] );
		}		
	} else {
		$slug = "kprl-staffing-multi-0";
	}
	
	echo '<span id="' . $slug . '" class="kprl-staffing-list-parent">';
	
	if ( $a['heading'] !== NULL ) {
		echo '<' . $lt . ' style="' . $boco . '" class="kprl-staffing-list-title"><a href="#' . $slug . '">' . $a['heading'] . '</a></' . $lt . '>';
	}

	
	foreach($persons as $person) {
		
		foreach($person as $p) {
			
			echo kprl_staffing_display_person( $p['pid'], $colcss, $a['imgsize'], $a['minheight'], $pt, $p['class'] );
			
		}
		
	}
	
	echo '</span>';
	
	if ( $wrap ) { echo '</div>'; }
	
} else {
	
	foreach($custom_terms as $custom_term) {
		
		$termchildren = array();
		$term_childs = array();
		
		if ( $childs ) {
			$termchildren = get_terms( 
				$taxName,
				array(
					'child_of' => $custom_term->term_id,
					'orderby' => $a['child_orderby'],
					'order' => $a['order'],
				)
			);
			
			foreach($termchildren as $termchild) {
				$term_childs[] = $termchild->term_id;
			}
		}
		
		$args = array(
			'post_type' => 'kprl-staff',
			'tax_query' => array(
				array(
					'taxonomy' => $taxName,
					'field' => 'term_id',
					'terms' => $custom_term->term_id,
					'include_children' => $childs,
				),
				array(
					'taxonomy' => $taxName,
					'field' => 'id',
					'terms' => $term_childs,
					'operator' => 'NOT IN',
				),
			),
			'orderby' => $a['orderby'],
			'order' => $a['order'],
			'posts_per_page' => -1,
		);
		
		$loop = new WP_Query($args);
		if($loop->have_posts()) {
			
			if ( $a['heading'] !== NULL ) {
				$heading = $a['heading'];
				if ( $a['headingid'] !== NULL ) {
					$slug = sanitize_title( $a['headingid'] );
				} else {
					$slug = sanitize_title( $a['heading'] );
				}
			} else {
				$heading = $custom_term->name;
				if ( $a['headingid'] !== NULL ) {
					$slug = sanitize_title( $a['headingid'] );
				} else {
					$slug = $custom_term->slug;
				}
			}
			
			if ( $compact ) {
				$cssss2 = "kprl-staffing-list-compact " . $cssss;
				$chldss = "kprl-staffing-list-child";
			} else {
				$cssss2 = "kprl-staffing-list " . $cssss;
				$chldss = "";
			}
	
			if ( $wrap ) { echo '<div id="' . $cssss . '-' . $custom_term->term_id . '" class="' . $cssss2 . '">'; }
				
				echo '<span id="' . $slug . '" class="kprl-staffing-list-parent">';
				
				if ( $noheading == FALSE ) { 
					
					echo '<' . $lt . ' style="' . $boco . '" class="kprl-staffing-list-title"><a href="#' . $slug . '">' . $heading . '</a>';
					echo '<span class="pull-right">';
					foreach($term_childs as $tc) { //array_reverse($term_childs)
						$tcs = get_term_by( 'term_id', $tc, $taxName );
						$childslug = $slug . "-" . $tcs->slug;
						echo '<a href="#' .  $childslug . '" class="kprl-staffing-list-title-button">' .  $tcs->name . '</a>';
					}
					echo '</span>';
					echo '</' . $lt . '>';
					
				}
				
					while($loop->have_posts()) : $loop->the_post();
					
						echo kprl_staffing_display_person( get_the_ID(), $colcss, $a['imgsize'], $a['minheight'], $pt, "" );
						
					endwhile;
					
				echo '</span>';
	
				foreach($term_childs as $tc) {
					
					$tcs = get_term_by( 'term_id', $tc, $taxName );
					
					$args = array('post_type' => 'kprl-staff',
						'tax_query' => array(
							array(
								'taxonomy' => $taxName,
								'field' => 'term_id',
								'terms' => $tcs->term_id,
							),
						),
						'orderby' => $a['child_orderby'],
						'order' => $a['order'],
						'posts_per_page' => -1,
					);
					
					$loop = new WP_Query($args);
					if($loop->have_posts()) {
						
						$childslug = $slug . "-" . $tcs->slug;
			
						echo '<span id="' . $childslug . '" class="kprl-staffing-list-child">';
							
							if ( $compact == FALSE ) {
								echo '<' . $lct . ' class="kprl-staffing-list-child-title"><a href="#' . $childslug . '">' . $tcs->name . '</a></' . $lct . '>';
							}
						
							while($loop->have_posts()) : $loop->the_post();
							
								echo kprl_staffing_display_person( get_the_ID(), $colcss, $a['imgsize'], $a['minheight'], $pt, "" );
								
							endwhile;
						
						echo '</span>';
						
					}
					wp_reset_query();
				}
			
			if ( $wrap ) { echo '</div>'; }
			
		}
		wp_reset_query();
	}
	
}