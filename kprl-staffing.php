<?php
/**
 * Plugin Name: kprl Bemanning
 * Plugin URI: https://bitbucket.org/kprl/kprl-staffing
 * Bitbucket Plugin URI: https://bitbucket.org/kprl/kprl-staffing
 * Description: Functionality to manage, list and administer staff and individuals based on departments and other categories and criterias.
 * Version: 1.1.0
 * Author: Karl Lettenström
 * Author URI: http://kprl.se
 * Text Domain: kprl-staffing
 * Domain Path: Optional. Plugin's relative directory path to .mo files. Example: /locale/
 * Network: Optional. Whether the plugin can only be activated network wide. Example: true
 * License: GPL2
 */

/*  Copyright 2018 Karl Lettenström (email : karl@kprl.se)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License, version 2, as
    published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

if ( ! defined( 'KPRL_STAFFING_PLUGIN_VERSION' ) ) {
	define( 'KPRL_STAFFING_PLUGIN_VERSION', '1.1.0' );
}

if ( ! defined( 'KPRL_STAFFING_PLUGIN_NAME' ) ) {
	define( 'KPRL_STAFFING_PLUGIN_NAME', 'kprl Bemanning' );
}

//core-files
include 'core/functions.php';
include 'core/options.php';

//custom post types
include 'cpt/staff.php';

//taxonomies
include 'taxonomies/categories.php';
include 'taxonomies/departments.php';

//views
// kprl_staffing_views_add( ID, "NAME", "DESCRIPTION" );
kprl_staffing_views_add( 3, "Display-Person", "Visa enskild person" );
kprl_staffing_views_add( 4, "List-persons", "Visa som lista av personer" );

//widgets
//include 'widget/kprl-staffing.php';

//shortcodes
include 'shortcodes/kprl-staffing.php';

function kprl_staffing_style() {
	wp_register_style( 'kprl-staffing-style', plugins_url() . '/kprl-staffing/assets/kprl-staffing-style.css', false, KPRL_STAFFING_PLUGIN_VERSION );
    wp_enqueue_style( 'kprl-staffing-style' );
}
add_action( 'wp_enqueue_scripts', 'kprl_staffing_style' );

add_action( 'admin_enqueue_scripts', 'kprl_staffing_enqueue_color_picker' );
function kprl_staffing_enqueue_color_picker( $hook_suffix ) {
    // first check that $hook_suffix is appropriate for your admin page
    wp_enqueue_style( 'wp-color-picker' );
    wp_enqueue_script( 'kprl_color-picker-handle', plugins_url('assets/color-picker.js', __FILE__ ), array( 'wp-color-picker' ), KPRL_STAFFING_PLUGIN_VERSION, true );
}
