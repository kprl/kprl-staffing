<?php
// Creating the widget
class kprl_staffing_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
		// Base ID of your widget
		'kprl_staffing_widget',

		// Widget name will appear in UI
		__('kprl Bemanning', 'kprl_staffing_widget_domain'),

		// Widget description
		array( 'description' => __( 'En widget som listar och/eller visar bemanning', 'kprl_staffing_widget_domain' ), ) 
		);
	}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		$a['type'] = $instance['type'];
		$a['dep'] = $instance['dep'];
		$a['cat'] = $instance['cat'];
		$a['person'] = $instance['person'];

		$a['imgsize'] = $instance['imgsize'];
		$a['height'] = $instance['height'];
		$a['childs'] = $instance['childs'];
		$a['controls'] = $instance['controls'];

		$a['list-title'] = $instance['list-title'];
		$a['list-child-title'] = $instance['list-child-title'];
		$a['person-title'] = $instance['person-title'];

		if ( isset($instance['orderby']) ) { $a['orderby'] = $instance['orderby']; } else { $a['orderby'] = kprl_staffing_get_option("orderby"); }

		if ( $a['orderby'] == "sort" ) {
			$a['orderby'] = NULL;
		}

		$a['class'] = "widget";

		// before and after widget arguments are defined by themes
		echo $args['before_widget'];
		if ( ! empty( $title ) )
		echo $args['before_title'] . $title . $args['after_title'];

		// This is where you run the code and display the output

		$custom_terms[0] = get_term_by( 'term_id', $a['dep'], 'staff-departments' );
		$a['class'] += " sc-dep";
		$taxName = "staff-departments";
		$cssss = "kprl-staffing-departments";
		$uniqueid = "dep" . $a['dep'];

		$getviewtype = partnerlist_views_get($a['type']);

		$dir = plugin_dir_path( __FILE__ );
		require $dir . "../views/" . $getviewtype['slug'] . ".php";

		//echo $args['after_widget'];
	}

	// Widget Backend
	public function form( $instance ) {

		// Check values
		if( $instance ) {
			$title            = esc_attr($instance['title']);
			$type             = esc_attr($instance['type']);
			$dep              = esc_attr($instance['dep']);
			$cat              = esc_attr($instance['cat']);
			$person           = esc_attr($instance['person']);
			$imgsize          = esc_attr($instance['imgsize']);
			$height           = esc_attr($instance['height']);
			$childs           = esc_attr($instance['childs']);
			$controls         = esc_attr($instance['controls']);
			$list_title       = esc_attr($instance['list-title']);
			$list_child_title = esc_attr($instance['list-child-title']);
			$person_title     = esc_attr($instance['person-title']);
			$orderby          = esc_attr($instance['orderby']);
		} else {
			$title            = __( 'Avdelning', 'kprl_staffing_widget_domain' );
			$type             = __( 'carousel-block', 'kprl_staffing_widget_domain' );
			$dep              = __( 'all', 'kprl_staffing_widget_domain' );
			$cat              = __( 'all', 'kprl_staffing_widget_domain' );
			$person           = __( NULL, 'kprl_staffing_widget_domain' );
			$imgsize          = __( 'kprl_staffing_thumb', 'kprl_staffing_widget_domain' );
			$height           = __( 320, 'kprl_staffing_widget_domain' );
			$childs           = __( 'show', 'kprl_staffing_widget_domain' );
			$controls         = __( 'hide', 'kprl_staffing_widget_domain' );
			$list_title       = __( 'h1', 'kprl_staffing_widget_domain' );
			$list_child_title = __( 'h2', 'kprl_staffing_widget_domain' );
			$person_title     = __( 'h4', 'kprl_staffing_widget_domain' );
			$orderby          = __( kprl_staffing_get_option("orderby"), 'kprl_staffing_widget_domain' );
		}

		kprl_staffing_add_widget_field("text", $this->get_field_id('title'), $this->get_field_name('title'), "Titel:", $title);

		kprl_staffing_add_widget_field("select", $this->get_field_id('departments'), $this->get_field_name('departments'), "Avdelning att visa:", $departments, array_departments());

		kprl_staffing_add_widget_field("select", $this->get_field_id('blogtitle'), $this->get_field_name('blogtitle'), "Heading:", $blogtitle, array_headingstyles());

		kprl_staffing_add_widget_field("select", $this->get_field_id('imgsize'), $this->get_field_name('imgsize'), "Storlek på bilderna:", $imgsize, array_imagesizes());

		//$controlArr = Array(Array('Dölj navigeringspilarna', 'hide'), Array('Visa navigeringspilarna', 'show'));
		//partnerlist_add_widget_field("select", $this->get_field_id('controls'), $this->get_field_name('controls'), "Hantering av navigeringspilar:", $controls, $controlArr);

		kprl_staffing_add_widget_field("select", $this->get_field_id('orderby'), $this->get_field_name('orderby'), "Ordning av partners:", $orderby, array_orderby());

	}

	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();

		$instance['title']            = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['type']             = ( ! empty( $new_instance['type'] ) ) ? strip_tags( $new_instance['type'] ) : '';
		$instance['dep']              = ( ! empty( $new_instance['dep'] ) ) ? strip_tags( $new_instance['dep'] ) : '';
		$instance['cat']              = ( ! empty( $new_instance['cat'] ) ) ? strip_tags( $new_instance['cat'] ) : '';
		$instance['person']           = ( ! empty( $new_instance['person'] ) ) ? strip_tags( $new_instance['person'] ) : '';
		$instance['imgsize']          = ( ! empty( $new_instance['imgsize'] ) ) ? strip_tags( $new_instance['imgsize'] ) : '';
		$instance['height']           = ( ! empty( $new_instance['height'] ) ) ? strip_tags( $new_instance['height'] ) : '';
		$instance['childs']           = ( ! empty( $new_instance['childs'] ) ) ? strip_tags( $new_instance['childs'] ) : '';
		$instance['controls']         = ( ! empty( $new_instance['controls'] ) ) ? strip_tags( $new_instance['controls'] ) : '';
		$instance['list-title']       = ( ! empty( $new_instance['list-title'] ) ) ? strip_tags( $new_instance['list-title'] ) : '';
		$instance['list-child-title'] = ( ! empty( $new_instance['list-child-title'] ) ) ? strip_tags( $new_instance['list-child-title'] ) : '';
		$instance['person-title']     = ( ! empty( $new_instance['person-title'] ) ) ? strip_tags( $new_instance['person-title'] ) : '';
		$instance['orderby']          = ( ! empty( $new_instance['orderby'] ) ) ? strip_tags( $new_instance['orderby'] ) : '';

		return $instance;
	}
} // Class kprl_staffing_widget ends here

// Register and load the widget
function kprl_staffing_load_widget() {
	register_widget( 'kprl_staffing_widget' );
}
add_action( 'widgets_init', 'kprl_staffing_load_widget' );
