<?php

// Register kprl_staffing_staff
function kprl_staffing_staff() {

	$labels = array(
		'name'                => _x( 'Personer', 'Post Type General Name', 'wordpress' ),
		'singular_name'       => _x( 'Person', 'Post Type Singular Name', 'wordpress' ),
		'menu_name'           => __( 'Personer', 'wordpress' ),
		'parent_item_colon'   => __( 'Föräldraperson:', 'wordpress' ),
		'all_items'           => __( 'Personer', 'wordpress' ),
		'view_item'           => __( 'Visa personer', 'wordpress' ),
		'add_new_item'        => __( 'Lägg till ny person', 'wordpress' ),
		'add_new'             => __( 'Lägg till', 'wordpress' ),
		'edit_item'           => __( 'Redigera person', 'wordpress' ),
		'update_item'         => __( 'Uppdatera person', 'wordpress' ),
		'search_items'        => __( 'Sök person', 'wordpress' ),
		'not_found'           => __( 'Ingen person hittades', 'wordpress' ),
		'not_found_in_trash'  => __( 'Ingen person hittades i papperskorgen', 'wordpress' ),
	);
	
	$rewrite = array(
		'slug'                => 'staff',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	
	$args = array(
		'label'               => __( 'Bemanning', 'wordpress' ),
		'description'         => __( 'Används för att skapa hantera, lista och rotera bemanning och personer.', 'wordpress' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'thumbnail' ),
		'taxonomies'          => array( ),
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => 'kprl-staffing',
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'page',
	);
		
	register_post_type( 'kprl-staff', $args );

}

// Hook into the 'init' action
add_action( 'init', 'kprl_staffing_staff', 0 );

/**
 * Adds a box to the main column on the Post and Page edit screens.
 */
function kprl_staffing_staff_add_meta_box() {
	
	add_meta_box(
		'kprl_staffing_meta_box',
		__( 'Alternativ och inställningar', 'wordpress' ),
		'kprl_staffing_staff_meta_box_callback',
		'kprl-staff', 
		'normal'
	);
	
}
add_action( 'add_meta_boxes', 'kprl_staffing_staff_add_meta_box' );

/**
 * Prints the box content.
 * 
 * @param WP_Post $post The object for the current post/page.
 */
function kprl_staffing_staff_meta_box_callback( $post ) {
	
	// Add an nonce field so we can check for it later.
	wp_nonce_field( 'kprl_staffing_staff_meta', 'kprl_staffing_staff_meta_nonce' );
	
	$kprl_staffing_staff_worktitle = get_post_meta($post->ID, 'kprl_staffing_staff_worktitle', true);
	kprl_staffing_add_meta_field( $post->ID, 'kprl_staffing_staff_worktitle', 'Jobbtitel', 'input', $kprl_staffing_staff_worktitle );
	
	$kprl_staffing_staff_phone = get_post_meta($post->ID, 'kprl_staffing_staff_phone', true);
	kprl_staffing_add_meta_field( $post->ID, 'kprl_staffing_staff_phone', 'Telefonnummer (Flera nummer separeras med kommatecken)', 'input', $kprl_staffing_staff_phone );
	
	$kprl_staffing_staff_email = get_post_meta($post->ID, 'kprl_staffing_staff_email', true);
	if ( $kprl_staffing_staff_email == NULL ) { $kprl_staffing_staff_email = "fornamn.efternamn@vf.se"; }
	kprl_staffing_add_meta_field( $post->ID, 'kprl_staffing_staff_email', 'Epostadress ("fornamn.efternamn@vf.se" ersätts automatiskt av angivet för.efternamn)', 'input', $kprl_staffing_staff_email );

}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function kprl_staffing_staff_save_meta_data( $post_id ) {

	/*
	 * We need to verify this came from our screen and with proper authorization,
	 * because the save_post action can be triggered at other times.
	 */

	// Check if our nonce is set.
	if ( ! isset( $_POST['kprl_staffing_staff_meta_nonce'] ) ) {
		return;
	}

	// Verify that the nonce is valid.
	if ( ! wp_verify_nonce( $_POST['kprl_staffing_staff_meta_nonce'], 'kprl_staffing_staff_meta' ) ) {
		return;
	}

	// If this is an autosave, our form has not been submitted, so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	// Check the user's permissions.
	if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

		if ( ! current_user_can( 'edit_page', $post_id ) ) {
			return;
		}

	} else {

		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
	}

	/* OK, it's safe for us to save the data now. */
	
	kprl_staffing_save_meta_field($post_id, 'kprl_staffing_staff_worktitle', 'input');
	kprl_staffing_save_meta_field($post_id, 'kprl_staffing_staff_phone', 'input');
	kprl_staffing_save_meta_field($post_id, 'kprl_staffing_staff_email', 'input');

}
add_action( 'save_post', 'kprl_staffing_staff_save_meta_data' );

function kprl_staffing_staff_change_title_text( $title ){
	$screen = get_current_screen();
	if  ( 'kprl-staff' == $screen->post_type ) {
		$title = 'Ange namnet på personen här';
	}
	return $title;
}
add_filter( 'enter_title_here', 'kprl_staffing_staff_change_title_text' );

add_filter('manage_kprl-staff_posts_columns', 'kprl_staff_table_head');
function kprl_staff_table_head( $defaults ) {
	$defaults['jobbtitel'] = 'Jobbtitel';
	$defaults['shortcode'] = 'Shortcode';
	$defaults['postid'] = 'ID#';
	return $defaults;
}

add_action( 'manage_kprl-staff_posts_custom_column', 'kprl_staff_table_content', 10, 2 );
function kprl_staff_table_content( $column_name, $post_id ) {
	
	if ($column_name == 'jobbtitel') {
		$status = get_post_meta( $post_id, 'kprl_staffing_staff_worktitle', true );
		echo $status;
	}
	
	if ($column_name == 'shortcode') {
		echo "<input style='width:100%' type='text' name='sc" . $post_id . "' value='[kprl-staffing person=" . $post_id . "]'>";
	}
	
	if ($column_name == 'postid') {
		echo $post_id;
	}

}