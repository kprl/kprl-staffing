<?php
	
function kprl_staffing_shortcode_func( $atts ) {
	
	ob_start();
	
	$a = shortcode_atts( array(
		'type'             => NULL, 
		'heading'		   => NULL, 
		'headingid'		   => NULL, 
		'dep'              => 'all', 
		'cat'              => 'all', 
		'person'           => NULL, 
		'multi'            => NULL, 
		'hide'             => NULL, 
		'imgsize'          => kprl_staffing_get_option("imgsize"), 
		'list-title'       => kprl_staffing_get_option("list_title"), 
		'list-child-title' => kprl_staffing_get_option("list_child_title"), 
		'person-title'     => kprl_staffing_get_option("person_title"), 
		'orderby'          => kprl_staffing_get_option("orderby"), 
		'child_orderby'    => kprl_staffing_get_option("child_orderby"), 
		'order'            => kprl_staffing_get_option("order"), 
		'height'           => kprl_staffing_get_option("height"), 
		'minheight'        => kprl_staffing_get_option("minheight"), 
		'class'            => 'shortcode', 
		'colcss'           => kprl_staffing_get_option("colcss"), 
		'boco'        	   => NULL, 
	), $atts );
	
	$custom_terms = array();
	$persons = array();
	$PoT = FALSE;
	$stack = FALSE;
	$wrap = TRUE;
	$compact = FALSE;
	$childs = TRUE;
	$noheading = FALSE;
	
	foreach ( $atts as $attcheck ) {
		if ( $attcheck == "stack" ) { $stack = TRUE; }
		if ( $attcheck == "nowrap" ) { $wrap = FALSE; }
		if ( $attcheck == "compact" ) { $compact = TRUE; }
		if ( $attcheck == "nochilds" ) { $childs = FALSE; }
		if ( $attcheck == "noheading" ) { $noheading = TRUE; }
	}
	
	$colcss = $a['colcss'];
	
	$lt = $a['list-title'];
	$lct = $a['list-child-title'];
	$pt = $a['person-title'];
	
	$cssss = "kprl-staffing-persons";
	
	if ( $a['orderby'] == "sort" ) {
		$a['orderby'] = NULL;
	}
	
	if ( $a['child_orderby'] == "sort" ) {
		$a['child_orderby'] = NULL;
	}
	
	$type = "list-persons";
	
	$boco = $a['boco'];
	
	if ( is_numeric( $a['dep'] ) ) {
		
		$custom_terms[0] = get_term_by( 'term_id', $a['dep'], 'staff-departments' );
		$a['class'] += " sc-dep";
		$taxName = "staff-departments";
		$cssss = "kprl-staffing-departments";
		$uniqueid = "dep" . $a['dep'];
		if ( $a['boco'] == NULL ) { $boco = kprl_staffing_get_option("depboco"); }
		
	} else if ( is_numeric( $a['cat'] ) ) {
		
		$custom_terms[0] = get_term_by( 'term_id', $a['cat'], 'staff-categories' );
		$a['class'] += " sc-cat";
		$taxName = "staff-categories";
		$cssss = "kprl-staffing-categories";
		$uniqueid = "cat" . $a['cat'];
		if ( $a['boco'] == NULL ) { $boco = kprl_staffing_get_option("catboco"); }
		
	} else if ( is_numeric( $a['person'] ) ) {
		
		$person_id = $a['person'];
		$a['class'] += " sc-person";
		
		$type = "display-person";
		
	} else if ( $a['multi'] ) {
		
		$mids = explode(",", $a['multi']);
		$i = 0;
		
		foreach ($mids as $mid) {
			
			if ( FALSE === get_post_status( trim($mid) ) ) {
				// The post does not exist
				
				$theterm = NULL;
				$theterm = get_term(trim($mid));
				
				if ( $theterm !== NULL ) {
					
					if ( property_exists($theterm, 'taxonomy') ) {
					
						$args = array(
							'post_type' => 'kprl-staff',
							'tax_query' => array(
								array(
									'taxonomy' => $theterm->taxonomy,
									'field' => 'term_id',
									'terms' => trim($mid),
								),
							),
							'orderby' => $a['orderby'],
							'order' => $a['order'],
							'posts_per_page' => -1,
						);
						
						$loop = new WP_Query($args);
						if($loop->have_posts()) {
					
							while($loop->have_posts()) : $loop->the_post();
							
								if ( $stack == TRUE ) { $i = get_the_ID(); }
								
								$persons[$i][get_the_ID()]['pid'] = get_the_ID();
								$persons[$i][get_the_ID()]['class'] = "kprl-" . $theterm->taxonomy . "-person";
								
							endwhile;
							
						}
						wp_reset_query();
						
					}
					
				}
				
			} else {
				// The post exists
				
				if ( is_singular( 'kprl-staff' ) ) {
				
					if ( $stack == TRUE ) { $i = trim($mid); }
					
					$persons[$i][trim($mid)]['pid'] = trim($mid);
					$persons[$i][trim($mid)]['class'] = "kprl-staff-persons-person";
					
				}
			}
			
			$i++;	
			
		}
		
		if ( $a['boco'] == NULL ) { $boco = kprl_staffing_get_option("depboco"); }
		$PoT = TRUE;
		
	} else {
		
		if ( $a['cat'] == "all" OR $a['cat'] == 1 ) {
			$custom_terms = get_terms( array( 'taxonomy' => 'staff-categories', 'hide_empty' => false, ) );
		} else {
			$custom_terms[0] = get_term_by( 'term_id', $a['cat'], 'staff-categories' );
		}
		
		$uniqueid = "cat" . $a['cat'];
		
	}
	
	if ($a['hide']) {
		foreach($custom_terms as $key => $cstmtrm) {
			if ($a['hide'] == $cstmtrm->term_id) {
				unset($custom_terms[$key]);
			}
		}
	}
	
	if ( $a['type'] !== NULL ) {
		$type = $a['type'];
	}
	
	$boco = "border-color: " . $boco . ";";
	
	$getviewtype = kprl_staffing_views_get($type);
	$dir = plugin_dir_path( __FILE__ );
	require $dir . "../views/" . $getviewtype['slug'] . ".php";
	
	return ob_get_clean();
	
}
add_shortcode( 'kprl-staffing', 'kprl_staffing_shortcode_func' );