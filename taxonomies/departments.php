<?php
	
add_action( 'init', 'staff_departments_taxonomies', 0 );
function staff_departments_taxonomies() {
	
	$labels = array(
		'name'              => _x( 'Avdelningar', 'taxonomy general name' ),
		'singular_name'     => _x( 'Avdelning', 'taxonomy singular name' ),
		'search_items'      => __( 'Sök avdelning' ),
		'all_items'         => __( 'Alla avdelningar' ),
		'parent_item'       => __( 'Föräldravdelning' ),
		'parent_item_colon' => __( 'Föräldravdelning:' ),
		'edit_item'         => __( 'redigera avdelning' ),
		'update_item'       => __( 'Uppdatera avdelning' ),
		'add_new_item'      => __( 'Lägg till ny avdelning' ),
		'new_item_name'     => __( 'Ny avdelning' ),
		'menu_name'         => __( 'Avdelningar' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'staff-departments' ),
		'public'            => false,
	);

	register_taxonomy( 'staff-departments', 'kprl-staff', $args );
}

function staff_departments_taxonomy_columns( $columns )
{
	$columns['shortcode'] = __('Shortcode');
	$columns['termid'] = __('ID#');
	return $columns;
}
add_filter('manage_edit-staff-departments_columns' , 'staff_departments_taxonomy_columns');

function staff_departments_taxonomy_columns_content( $content, $column_name, $term_id )
{
	if ($column_name == 'shortcode') {
		echo "<input style='width:100%' type='text' name='sc" . $term_id . "' value='[kprl-staffing dep=" . $term_id . "]'>";
	}
	
	if ($column_name == 'termid') {
		echo $term_id;
	}
	return $content;
}
add_filter( 'manage_staff-departments_custom_column', 'staff_departments_taxonomy_columns_content', 10, 3 );