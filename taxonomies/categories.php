<?php
	
add_action( 'init', 'staff_categories_taxonomies', 0 );
function staff_categories_taxonomies() {
	
	$labels = array(
		'name'              => _x( 'Bemanningskategorier', 'taxonomy general name' ),
		'singular_name'     => _x( 'Bemanningskategori', 'taxonomy singular name' ),
		'search_items'      => __( 'Sök bemanningskategori' ),
		'all_items'         => __( 'Alla bemanningskategorier' ),
		'parent_item'       => __( 'Föräldrakategori' ),
		'parent_item_colon' => __( 'Föräldrakategori:' ),
		'edit_item'         => __( 'redigera bemanningskategori' ),
		'update_item'       => __( 'Uppdatera bemanningskategori' ),
		'add_new_item'      => __( 'Lägg till ny bemanningskategori' ),
		'new_item_name'     => __( 'Ny bemanningskategori' ),
		'menu_name'         => __( 'Bemanningskategorier' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'staff-categories' ),
		'public'            => false,
	);

	register_taxonomy( 'staff-categories', 'kprl-staff', $args );
}

function staff_categories_taxonomy_columns( $columns )
{
	$columns['shortcode'] = __('Shortcode');
	$columns['termid'] = __('ID#');
	return $columns;
}
add_filter('manage_edit-staff-categories_columns' , 'staff_categories_taxonomy_columns');

function staff_categories_taxonomy_columns_content( $content, $column_name, $term_id )
{
	if ($column_name == 'shortcode') {
		echo "<input style='width:100%' type='text' name='sc" . $term_id . "' value='[kprl-staffing cat=" . $term_id . "]'>";
	}
	
	if ($column_name == 'termid') {
		echo $term_id;
	}
	return $content;
}
add_filter( 'manage_staff-categories_custom_column', 'staff_categories_taxonomy_columns_content', 10, 3 );