<?php

/*** Hanterar de "views" som finns tillgängliga. ***/
function kprl_staffing_views() {
	global $kprl_staffing_views_array;
	ksort($kprl_staffing_views_array);
	return $kprl_staffing_views_array;
}

function kprl_staffing_views_add( $view_id, $name, $desc ) {
	global $kprl_staffing_views_array;
	$slug = sanitize_title($name);

	$kprl_staffing_views_array[$view_id]['view_id'] = $view_id;
	$kprl_staffing_views_array[$view_id]['name'] = $name;
	$kprl_staffing_views_array[$view_id]['slug'] = $slug;
	$kprl_staffing_views_array[$view_id]['desc'] = $desc;
}

function kprl_staffing_views_get( $input ) {
	global $kprl_staffing_views_array;

	foreach ( $kprl_staffing_views_array as $keysss => $plvA ) {
		if ($plvA['view_id'] == $input) {
			return $kprl_staffing_views_array[$keysss];
		}

		if ($plvA['slug'] == $input) {
			return $kprl_staffing_views_array[$keysss];
		}
	}
}

/*** Functions för att bygga arrays som potentiellt används på flertalet ställen och skall innehålla samma värden ***/
function array_orderby() {
	$orderbyArr = Array(
						Array('Ordna efter sortering (baserad på tredjepartsplugin)', 'sort'),
						Array('Ingen specifik ordning', 'none'),
						Array('Ordna efter ID (notera stora bokstäver)', 'ID'),
						Array('Ordna efter namnet på personen (the post title)', 'title'),
						Array('Ordna efter datum', 'date'),
						Array('Ordna efter ändringsdatum', 'modified'),
						Array('Slumpvis ordning', 'rand'),
						);

	return $orderbyArr;
}

function array_imagesizes() {
	$sizes = get_intermediate_image_sizes();
	$sizeArr = Array(Array('full', 'full'));
	foreach ( $sizes as $key => $siz ) {
		$sizeArr[$key+1][0] = $siz;
		$sizeArr[$key+1][1] = $siz;
	}

	return $sizeArr;
}

function array_headingstyles() {
	$headingArr = Array(
						Array('h1', 'h1'),
						Array('h2', 'h2'),
						Array('h3', 'h3'),
						Array('h4', 'h4'),
						Array('h5', 'h5'),
						Array('span', 'span'),
						Array('p', 'p'),
						Array('div', 'div'),
						Array('li', 'li'),
						);

	return $headingArr;
}

function array_departments() {
	$terms = get_terms( array( 'taxonomy' => 'staff-departments', 'hide_empty' => false, ) );
	$departmentsArr = Array(Array('Alla avdelningar', 1));
	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
		foreach ( $terms as $key => $term ) {
			$departmentsArr[$key+1][0] = $term->name;
			$departmentsArr[$key+1][1] = $term->term_id;
		}
	}

	return $departmentsArr;
}

function array_categories() {
	$terms = get_terms( array( 'taxonomy' => 'staff-categories', 'hide_empty' => false, ) );
	$categoriesArr = Array(Array('Alla bemanningskategorier', 1));
	if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
		foreach ( $terms as $key => $term ) {
			$categoriesArr[$key+1][0] = $term->name;
			$categoriesArr[$key+1][1] = $term->term_id;
		}
	}

	return $categoriesArr;
}

function kprl_staffing_gen_email_from_title($post_id, $domain = "@domain.se") {
	$title = sanitize_title(get_the_title($post_id));

	$title = str_replace("-",".",$title);

	$email = sanitize_email($title . $domain);

	return $email;
}

function kprl_staffing_get_option( $name ) {
	$options = get_option( 'kprl_staffing_settings', array() );
	if ( array_key_exists($name, $options) ) {
		return $options[$name];
	} else {
		$def = 'DEFAULT_KPRL_' . strtoupper($name);
		if ( defined( $def ) ) {
			return constant($def);
		} else {
			return NULL;
		}
	}
}

function kprl_staffing_add_settings_field( $name, $default = NULL, $type = "input", $value = NULL, $displayid = false) {

	$option = kprl_staffing_get_option($name);

	if ( $option !== NULL ) {
		$kprl_staffing_value = $option;
	} else {
		$kprl_staffing_value = $default;
	}

	$name = "kprl_staffing_settings[" . $name . "]";

	if ($type == "input") {
		echo '<input type="text" id="' . $name . '" name="' . $name . '" value="' . esc_attr( $kprl_staffing_value ) . '" class="widefat" />';
		echo "Defaultvärde: " . $default;
	} else if ($type == "dtp") {
		echo '<script>
	$(function() {
		$( "#' . $name . '" ).datepicker({dateFormat: "yy-mm-dd"});
	});
</script>';
		echo '<input type="text" id="' . $name . '" name="' . $name . '" value="' . esc_attr( date("Y-m-d", $kprl_staffing_value) ) . '" class="widefat" />';
		echo "Defaultvärde: " . $default;
	} else if ($type == "hidden") {
		echo '<input type="hidden" id="' . $name . '" name="' . $name . '" value="' . esc_attr( $kprl_staffing_value ) . '" class="widefat" />';
		echo "Defaultvärde: " . $default;
	} else if ($type == "text") {
		echo '<textarea id="' . $name . '" name="' . $name . '" class="widefat" rows="3" />' . esc_textarea( $kprl_staffing_value ) . '</textarea>';
		echo "Defaultvärde: " . $default;
	} else if ($type == "chck") {
		echo "<input type='checkbox' id='" . $name . "' name='" . $name . "' " . checked( esc_attr( $kprl_staffing_value ), 1 ) . " value='1'>";
		echo "Defaultvärde: " . $default;
	} else if ($type == "radio") {
		echo "<input type='radio' id='" . $name . "' name='" . $name . "' " . checked( esc_attr( $kprl_staffing_value ), 1 ) . " value='1'>";
		echo "Defaultvärde: " . $default;
	} else if ($type == "select") {
		echo "<select class='widefat' name='" . $name . "' id='" . $name . "'>";
			foreach ($value as $val) {
				if ( $displayid ) { $displid = $val[1] . " | "; } else { $displid = ""; }
				echo "<option value='" . $val[1] . "'" . selected( $kprl_staffing_value, $val[1], false ) . ">" . $displid . $val[0] . "</option>";
			}
		echo "</select>";
		echo "Defaultvärde: " . $default;
	} else if ($type == "colorpicker") {
		echo '<input type="text" id="' . $name . '" name="' . $name . '" value="' . esc_attr( $kprl_staffing_value ) . '" data-default-color="' . esc_attr( $default ) . '" class="color-field" />';
	}

}

/*** Functions för att skapa och spara form-fält ***/
function kprl_staffing_add_meta_field($post_id, $metaattr = "kprl_staffing_value", $metatext = "Text", $type = "input", $value = NULL) {

	if ( strpos( $metaattr, '[' ) !== false ) {

		$metaattr_ex1 = explode( "["  , $metaattr);
		$metaattr_ex2 = explode( "][" , $metaattr_ex1[1]);
		$metaattr_ex3 = explode( "]"  , $metaattr_ex2[0]);
		$metaattr_ex4 = explode( "]"  , $metaattr_ex1[2]);

		$metaattr_val1 = $metaattr_ex1[0];
		$metaattr_val2 = $metaattr_ex2[0];
		$metaattr_val3 = $metaattr_ex3[0];
		$metaattr_val4 = $metaattr_ex4[0];

		$kprl_staffing_value = get_post_meta( $post_id, $metaattr_val1, true );

		if ( isset( $kprl_staffing_value[$metaattr_val3][$metaattr_val4] ) ) {
			$kprl_staffing_value = $kprl_staffing_value[$metaattr_val3][$metaattr_val4];
		} else {
			$kprl_staffing_value = get_post_meta( $post_id, $metaattr, true );
		}

	} else {

		$kprl_staffing_value = get_post_meta( $post_id, $metaattr, true );

	}

	echo '<p><label for="' . $metaattr . '">';
	_e( $metatext, 'vfbaans' );
	echo '</label></p>';

	if ($type == "input") {
		if ( $kprl_staffing_value == NULL) {
			$kprl_staffing_value = $value;
		}

		echo '<input type="text" id="' . $metaattr . '" name="' . $metaattr . '" value="' . esc_attr( $kprl_staffing_value ) . '" class="widefat" />';
	} else if ($type == "dtp") {
		echo '<script>
	$(function() {
		$( "#' . $metaattr . '" ).datepicker({dateFormat: "yy-mm-dd"});
	});
</script>';
		echo '<input type="text" id="' . $metaattr . '" name="' . $metaattr . '" value="' . esc_attr( date("Y-m-d", $kprl_staffing_value) ) . '" class="widefat" />';
	} else if ($type == "hidden") {
		echo '<input type="hidden" id="' . $metaattr . '" name="' . $metaattr . '" value="' . esc_attr( $kprl_staffing_value ) . '" class="widefat" />';
	} else if ($type == "text") {
		echo '<textarea id="' . $metaattr . '" name="' . $metaattr . '" class="widefat" rows="3" />' . esc_textarea( $kprl_staffing_value ) . '</textarea>';
	} else if ($type == "chck") {
		echo "<input type='checkbox' id='" . $metaattr . "' name='" . $metaattr . "' " . checked( esc_attr( $kprl_staffing_value ), 1 ) . " value='1'>";
	} else if ($type == "radio") {
		echo "<input type='radio' id='" . $metaattr . "' name='" . $metaattr . "' " . checked( esc_attr( $kprl_staffing_value ), 1 ) . " value='1'>";
	} else if ($type == "select") {
		echo "<select class='widefat' name='" . $metaattr . "' id='" . $metaattr . "'>";
			foreach ($value as $val) { echo '<option value="' . $val[1] . '"' . selected( $kprl_staffing_value, $val[1], false ) . '>' . $val[0] . '</option>'; }
		echo '</select>';
	}

}

function kprl_staffing_save_meta_field($post_id, $metaattr = "kprl_staffing_value", $type = "input", $time = "00:00:01") {

	// Make sure that it is set.
	if ( !isset( $_POST[$metaattr] ) AND $type != "array" ) {
		return;
	} else {

		if ($type == "input") {
			// Sanitize user input.
			$kprl_staffing_value = sanitize_text_field( $_POST[$metaattr] );

			if ( $kprl_staffing_value == "fornamn.efternamn@vf.se" ) {
				$kprl_staffing_value = kprl_staffing_gen_email_from_title($post->ID);
			}

			update_post_meta( $post_id, $metaattr, $kprl_staffing_value );
		} else if ($type == "text") {
			update_post_meta(
			    $post_id,
			    $metaattr,
			    implode( "\n", array_map( 'sanitize_text_field', explode( "\n", $_POST[$metaattr] ) ) )
			);
		} else if ($type == "timestamp") {
			// Sanitize user input.
			$kprl_staffing_value = sanitize_text_field( $_POST[$metaattr] );
			update_post_meta( $post_id, $metaattr, strtotime($kprl_staffing_value . " " . $time) );
		} else if ($type == "array") {
			if ( isset($_POST[$metaattr]) ) {
				update_post_meta( $post_id, $metaattr, $_POST[$metaattr] );
			}
		}

	}

}

function kprl_staffing_add_widget_field($type = 'text', $id = NULL, $name = NULL, $text = NULL, $value = NULL, $input = NULL, $class = 'widefat') {

	if ($type == "text") {
		echo '<p>';
		echo '<label for="' . $id . '">' . _e($text, 'kprl_staffing_youtube_widget') . '</label>';
		echo '<input class="' . $class . '" id="' . $id . '" name="' . $name . '" type="' . $type . '" value="' . $value . '" />';
		echo '</p>';
	} else if ($type == "textarea") {
		echo '<p>';
		echo '<label for="' . $id . '">' . _e($text, 'kprl_staffing_youtube_widget') . '</label>';
		echo '<textarea id="' . $id . '" name="' . $name . '" class="' . $class . '">' . $value . '</textarea>';
		echo '</p>';
	} else if ($type == "checkbox") {
		echo '<p>';
		echo '<input class="' . $class . '" id="' . $id . '" name="' . $name . '" type="' . $type . '" value="1"' . checked( 1, $value, false ) . '/>';
		echo '<label for="' . $id . '">' . _e($text, 'kprl_staffing_youtube_widget') . '</label>';
		echo '</p>';
	} else if ($type == "select") {
		echo '<p>';
		echo '<label for="' . $id . '">' . _e($text, 'kprl_staffing_youtube_widget') . '</label>';
		echo '<select name="' . $name . '" id="' . $id . '" class="' . $class . '">';

		foreach ($input as $key => $option) {
			echo '<option value="' . $option[1]. '" id="' . $option[1] . '"', $value == $option[1] ? ' selected="selected"' : '', '>', $option[0], '</option>';
		}

		echo '</select>';
		echo '</p>';
	}

}

function kprl_staffing_display_person( $person_id, $colcss, $imgsize, $minheight, $pt, $class = "" ) {

	$output = '<div id="' . get_post_field( 'post_name', get_post( $person_id ) ) . '" class="kprl-staffing-person ' . $class . ' ' . $colcss . '" style="min-height:' . $minheight . 'px;">';

		if ( has_post_thumbnail( $person_id ) ) {
			$output .= get_the_post_thumbnail( $person_id, $imgsize, array('class' => 'img-responsive' ));
		}

		$output .= '<div class="kprl-staffing-person-body">';

		$staffing_meta_value = get_post_meta( $person_id );

			$output .= '<' . $pt . ' class="kprl-staffing-person-title">' . get_the_title( $person_id ) . '</' . $pt . '>';

			$worktitle = $staffing_meta_value['kprl_staffing_staff_worktitle'][0];
			if (isset($worktitle)) {
				$output .= '<span class="kprl-staffing-person-worktitle">' . $worktitle . '</span>';
			}

			$emails = explode(",", $staffing_meta_value['kprl_staffing_staff_email'][0]);
			if (isset($emails)) {
				$output .= '<span class="kprl-staffing-person-email">';
				foreach ( $emails as $key => $email ) {
					if ( $key >= 1 ) { $output .= ", "; }
					$output .= '<a href="mailto:' . trim($email) . '">' . trim($email) . '</a>';
				}
				$output .= '</span>';
			}

			$phones = explode(",", $staffing_meta_value['kprl_staffing_staff_phone'][0]);
			if (isset($phones)) {
				$output .= '<span class="kprl-staffing-person-phone">';
				foreach ( $phones as $key => $phone ) {
					if ( $key >= 1 ) { $output .= ", "; }
					$output .= '<a href="tel:' . trim($phone) . '">' . trim($phone) . '</a>';
				}
				$output .= '</span>';
			}

		$output .= '</div>';

	$output .= '</div>';

	return $output;

}
