<?php
	
add_image_size( 'kprl_staffing_thumb', 250, 250, array( 'left', 'top' ) );
add_image_size( 'kprl_staffing_thumb_big', 500, 500, array( 'left', 'top' ) );

/*** För en gemensam menykategori. ***/
function kprl_staffing_add_admin_menu() { 
	
    add_menu_page(
    	__(KPRL_STAFFING_PLUGIN_NAME, 'wordpress'),
    	__(KPRL_STAFFING_PLUGIN_NAME, 'wordpress'), 
    	'manage_options', 
    	'kprl-staffing', 
    	'', 
    	'dashicons-id'
    	);
		
	add_submenu_page(
		'kprl-staffing', 
		__('Bemanningskategori','wordpress'), 
		__('Bemanningskategori','wordpress'), 
		'manage_options', 
		'/edit-tags.php?taxonomy=staff-categories'
		);
	
	add_submenu_page(
		'kprl-staffing', 
		__('Avdelningar','wordpress'), 
		__('Avdelningar','wordpress'), 
		'manage_options', 
		'/edit-tags.php?taxonomy=staff-departments'
		);
		
	add_submenu_page( 
		'kprl-staffing', 
		'Inställningar och information', 
		'Inställningar och information', 
		'manage_options', 
		'kprl-staffing-options', 
		'kprl_staffing_options_page'
		);
		
	// highlight the proper top level menu
	function kprl_staffing_menu_correction($parent_file) {
		global $current_screen;
		$taxonomy=$current_screen->taxonomy;
		if ($taxonomy == 'staff-categories' || $taxonomy == 'staff-departments')
			$parent_file='kprl-staffing';
		return $parent_file;
	}
	add_action('parent_file', 'kprl_staffing_menu_correction');
		
}
add_action( 'admin_menu', 'kprl_staffing_add_admin_menu' );

// DEFAULT-VALUES

if ( ! defined( 'DEFAULT_KPRL_COLCSS' ) ) {
	define( 'DEFAULT_KPRL_COLCSS', 'col-md-3 col-sm-4 col-xs-6' );
	define( 'DEFAULT_KPRL_DEPBOCO', '#cf343b' );
	define( 'DEFAULT_KPRL_CATBOCO', '#8aa045' );
}

if ( ! defined( 'DEFAULT_KPRL_ORDERBY' ) ) {
	define( 'DEFAULT_KPRL_ORDERBY', 'sort' );
	define( 'DEFAULT_KPRL_CHILD_ORDERBY', 'sort' );
	define( 'DEFAULT_KPRL_ORDER', 'ASC' );
}

if ( ! defined( 'DEFAULT_KPRL_LIST_TITLE' ) ) {
	define( 'DEFAULT_KPRL_LIST_TITLE', 'h2' );
	define( 'DEFAULT_KPRL_LIST_CHILD_TITLE', 'h3' );
	define( 'DEFAULT_KPRL_PERSON_TITLE', 'h4' );
}

if ( ! defined( 'DEFAULT_KPRL_HEIGHT' ) ) {
	define( 'DEFAULT_KPRL_HEIGHT', 320 );
	define( 'DEFAULT_KPRL_MINHEIGHT', 270 );
	define( 'DEFAULT_KPRL_IMGSIZE', 'kprl_staffing_thumb_big' );
}

add_action( 'admin_init', 'kprl_staffing_settings_init' );
function kprl_staffing_settings_init(  ) { 

	register_setting( 'kprl_staffing_settings_page', 'kprl_staffing_settings' );

	add_settings_section(
		'kprl_staffing_settings_page_section', 
		__( 'Inställningar för ' . KPRL_STAFFING_PLUGIN_NAME, 'kprl-staffing' ), 
		'kprl_staffing_settings_section_callback', 
		'kprl_staffing_settings_page'
	);

	add_settings_field( 
		'orderby', 
		__( 'orderby', 'kprl-staffing' ), 
		'kprl_staffing_setting_orderby_render', 
		'kprl_staffing_settings_page', 
		'kprl_staffing_settings_page_section' 
	);
	
	add_settings_field( 
		'child_orderby', 
		__( 'child_orderby', 'kprl-staffing' ), 
		'kprl_staffing_setting_child_orderby_render', 
		'kprl_staffing_settings_page', 
		'kprl_staffing_settings_page_section' 
	);
	
	add_settings_field( 
		'order', 
		__( 'order', 'kprl-staffing' ), 
		'kprl_staffing_setting_order_render', 
		'kprl_staffing_settings_page', 
		'kprl_staffing_settings_page_section' 
	);
	
	add_settings_field( 
		'list_title', 
		__( 'list_title', 'kprl-staffing' ), 
		'kprl_staffing_setting_list_title_render', 
		'kprl_staffing_settings_page', 
		'kprl_staffing_settings_page_section' 
	);
	
	add_settings_field( 
		'list_child_title', 
		__( 'list_child_title', 'kprl-staffing' ), 
		'kprl_staffing_setting_list_child_title_render', 
		'kprl_staffing_settings_page', 
		'kprl_staffing_settings_page_section' 
	);
	
	add_settings_field( 
		'person_title', 
		__( 'person_title', 'kprl-staffing' ), 
		'kprl_staffing_setting_person_title_render', 
		'kprl_staffing_settings_page', 
		'kprl_staffing_settings_page_section' 
	);
	
	add_settings_field( 
		'minheight', 
		__( 'minheight', 'kprl-staffing' ), 
		'kprl_staffing_setting_minheight_render', 
		'kprl_staffing_settings_page', 
		'kprl_staffing_settings_page_section' 
	);
	
	add_settings_field( 
		'imgsize', 
		__( 'imgsize', 'kprl-staffing' ), 
		'kprl_staffing_setting_imgsize_render', 
		'kprl_staffing_settings_page', 
		'kprl_staffing_settings_page_section' 
	);
	
	add_settings_field( 
		'colcss', 
		__( 'colcss', 'kprl-staffing' ), 
		'kprl_staffing_setting_colcss_render', 
		'kprl_staffing_settings_page', 
		'kprl_staffing_settings_page_section' 
	);
	
	add_settings_field( 
		'depboco', 
		__( 'depboco', 'kprl-staffing' ), 
		'kprl_staffing_setting_depboco_render', 
		'kprl_staffing_settings_page', 
		'kprl_staffing_settings_page_section' 
	);
	
	add_settings_field( 
		'catboco', 
		__( 'catboco', 'kprl-staffing' ), 
		'kprl_staffing_setting_catboco_render', 
		'kprl_staffing_settings_page', 
		'kprl_staffing_settings_page_section' 
	);

}

function kprl_staffing_setting_orderby_render(  ) { 
	kprl_staffing_add_settings_field( "orderby", DEFAULT_KPRL_ORDERBY, "select", array_orderby(), true);
}

function kprl_staffing_setting_child_orderby_render(  ) { 
	kprl_staffing_add_settings_field( "child_orderby", DEFAULT_KPRL_CHILD_ORDERBY, "select", array_orderby(), true);
}

function kprl_staffing_setting_order_render(  ) { 
	kprl_staffing_add_settings_field( "order", DEFAULT_KPRL_ORDER, "select", array(array("ASC", "ASC"), array("DESC", "DESC")));
}

function kprl_staffing_setting_list_title_render(  ) { 
	kprl_staffing_add_settings_field( "list_title", DEFAULT_KPRL_LIST_TITLE, "select", array_headingstyles());
}

function kprl_staffing_setting_list_child_title_render(  ) { 
	kprl_staffing_add_settings_field( "list_child_title", DEFAULT_KPRL_LIST_CHILD_TITLE, "select", array_headingstyles());
}

function kprl_staffing_setting_person_title_render(  ) { 
	kprl_staffing_add_settings_field( "person_title", DEFAULT_KPRL_PERSON_TITLE, "select", array_headingstyles());
}

function kprl_staffing_setting_minheight_render(  ) { 
	kprl_staffing_add_settings_field( "minheight", DEFAULT_KPRL_MINHEIGHT, "input");
}

function kprl_staffing_setting_imgsize_render(  ) { 
	kprl_staffing_add_settings_field( "imgsize", DEFAULT_KPRL_IMGSIZE, "select", array_imagesizes());
}

function kprl_staffing_setting_colcss_render(  ) { 
	kprl_staffing_add_settings_field( "colcss", DEFAULT_KPRL_COLCSS, "input");
}

function kprl_staffing_setting_depboco_render(  ) { 
	kprl_staffing_add_settings_field( "depboco", DEFAULT_KPRL_DEPBOCO, "colorpicker");
}

function kprl_staffing_setting_catboco_render(  ) { 
	kprl_staffing_add_settings_field( "catboco", DEFAULT_KPRL_CATBOCO, "colorpicker");
}

function kprl_staffing_settings_section_callback(  ) { 
	echo __( 'Ändra värde för de inställningar som finns tillgängliga.', 'kprl-staffing' );
}

// create custom plugin settings menu

function kprl_staffing_options_page() {
?>
	<div class="wrap">
		<h5 style="float:right;"><?php echo KPRL_STAFFING_PLUGIN_NAME . ", version " . KPRL_STAFFING_PLUGIN_VERSION; ?></h5>
		<h1>Information och Inställningar</h1>
		
		<h3>Shortcodes finns att finna för varje element som önskas visas ut på sajten.<h3>
		<h4>Exempel:</h4>
		<table class="widefat fixed striped" cellspacing="0">
			<tr><td>[kprl-staffing person=#person-ID]</td></tr>
			<tr><td>[kprl-staffing cat=#kategori-ID]</td></tr>
			<tr><td>[kprl-staffing dep=#avdelning-ID]</td></tr>
			<tr><td>[kprl-staffing multi='#ID,#ID,#ID']</td></tr>
		</table>
		
		<h3>Dessa går också att redigera vid annat önskat uppförande:</h3>
		<h4>Exempel:</h4>
		<table class="widefat fixed striped" cellspacing="0">
			<tr><td>[kprl-staffing cat=6 colcss='col-sm-4 col-xs-12' orderby='title' nowrap compact]</td></tr>
			<tr><td>[kprl-staffing dep=8 depboco='#cccccc' nochilds]</td></tr>
			<tr><td>[kprl-staffing person=136 colcss='col-sm-6 col-xs-12']</td></tr>
			<tr><td>[kprl-staffing multi='6,8,121,136,7,18,999' minheight='320' heading='Exempelgruppering' stack]</td></tr>
		</table>
		
		<h3>Lista över alla settings och dess definierade värden samt förklaring.</h3>
		<table class="widefat fixed striped" cellspacing="0">
			<thead>
				<tr>
					<td width="30%">Inställning</td>
					<td>Beskrivning</td>
				</tr>
			</thead>
			<tr>
				<td>heading='Rubrik för respektive cat eller dep'</td>
				<td>Om man vill använda en annan rubrik än den tillhörande.</td>
			</tr>
			<tr>
				<td>headingid='Genereras baserat på heading'</td>
				<td>Om man vill använda ett annat ID-tagg (inte ID#, utan ID-tagg som kan anropas som länk) än den automatgenererade.</td>
			</tr>
			<tr>
				<td>orderby='<?php echo kprl_staffing_get_option("orderby"); ?>'</td>
				<td>Ordning för hur personer listas. (Se separat lista nedan för mer info).</td>
			</tr>
			<tr>
				<td>child_orderby='<?php echo kprl_staffing_get_option("child_orderby"); ?>'</td>
				<td>Ordning för hur underkategorier och underavdelningar listas. (Se separat lista nedan för mer info).</td>
			</tr>
			<tr>
				<td>order='<?php echo kprl_staffing_get_option("order"); ?>'</td>
				<td>Sortering stigande (ASC) / fallande (DESC).</td>
			</tr>
			<tr>
				<td>list-title='<?php echo kprl_staffing_get_option("list_title"); ?>'</td>
				<td>Title-tag för kategorier och avdelningar.</td>
			</tr> 
			<tr>
				<td>list-child-title='<?php echo kprl_staffing_get_option("list_child_title"); ?>'</td>
				<td>Title-tag för underkategorier och underavdelningar.</td>
			</tr>
			<tr>
				<td>person-title='<?php echo kprl_staffing_get_option("person_title"); ?>'</td>
				<td>Title-tag för person.</td>
			</tr>
			<tr>
				<td>minheight='<?php echo kprl_staffing_get_option("minheight"); ?>'</span></td>
				<td>Minsta höjd (i px) för varje "personblock". Anges som ett numeriskt värde.</td>
			</tr>
			<tr>
				<td>imgsize='<?php echo kprl_staffing_get_option("imgsize"); ?>'</td>
				<td>Storlek/format på tumnagel. </td>
			</tr> 
			<tr>
				<td>colcss='<?php echo kprl_staffing_get_option("colcss"); ?>'</td>
				<td>Ange hur många cols som skall användas för varje person.</td>
			</tr>
			<tr>
				<td>boco='<?php echo kprl_staffing_get_option("depboco"); ?>'</td>
				<td>
					Ange färg för border (BOrder COlor). Värden som anges är hexadecimala och exklusive #.<br />
					standardfärg för dep: <font color="<?php echo kprl_staffing_get_option("depboco"); ?>"><?php echo kprl_staffing_get_option("depboco"); ?></font>, 
					standardfärg för cat: <font color="<?php echo kprl_staffing_get_option("catboco"); ?>"><?php echo kprl_staffing_get_option("catboco"); ?></font>.
				</td>
			</tr>
			<tr>
				<td>stack</td>
				<td>För "multi" kan du också välja att "stack" (stapla) personer eller ej. 
					<br />Alltså om du valt att visa kategorier och avdelningar där samma person kan förekomma i flera så kan du välja om dessa skall visas en gång, eller för varje gång de förekommer. 
					<br /><b>Ange bara "stack" i shortcode.</b></td>
			</tr>
			<tr>
				<td>nowrap</td>
				<td>Som standard finns det en "wrapper" runt output av personer, detta för att gruppera personerna för sig. 
					<br />Att inte ha denna wrapper innebär att man tillåter andra object att "flyta samman" med listan. 
					<br /><b>Ange bara "nowrap" i shortcode.</b></td>
			</tr>
			<tr>
				<td>compact</td>
				<td>För en mer kompakt visning där varje eventuell underavdelning (och/eller kategori) fortfarande skrivs ut, men istället för att hamna under en egen rubrik och heading så listas dem i samma stora lista som föräldraavdelningen. 
					<br /><b>Ange bara "compact" i shortcode.</b></td>
			</tr>
			<tr>
				<td>nochilds</td>
				<td>Om man vill lista en kategori eller avdelning, men inte dess eventuellt tillhörande "child". 
					<br /><b>Ange bara "nochild" i shortcode.</b></td>
			</tr>
			<tr>
				<td>noheading</td>
				<td>Om man vill visa en kategori eller avdelning utan att "heading" och tillhörande border skall visas. (Bör användas tillsammans med "compact" eller "nochilds" för att inte skapa en allt för stor förvirring.)
					<br /><b>Ange bara "noheading" i shortcode.</b></td>
			</tr>
		</table>
		
		<h3>Lista över de "orderby" som finns att tillgå:</h3>
		<table class="widefat fixed striped" cellspacing="0">
			<thead>
				<tr>
					<td width="30%">Namn</td>
					<td>Beskrivning</td>
				</tr>
			</thead>
			<?php 
				$array_orderby = array_orderby(); 
				foreach ( $array_orderby as $orderby ) {
					echo "<tr><td>" . $orderby[1] . "</td><td>" . $orderby[0] . "</td></tr>";
				}
			?>
		</table>
		
		<hr />
		
		<div id="settings">
		
			<form action='options.php' method='post'>
		
				<?php
					settings_fields( 'kprl_staffing_settings_page' );
					do_settings_sections( 'kprl_staffing_settings_page' );
					submit_button();
				?>
		
			</form>
		
		</div>
	
	</div>
<?php }